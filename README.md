# ifless-calculator

Demonstration of several design patterns creating a simple calculator, with no conditional statements (ifs).

## Design Patterns

On this project we make use of coding tecniques called "design patterns" to demonstrate a more structured approach to problem solving. This project was based on a real coding interview challenge done by me.

### State Pattern

Defines the high-level application flow, modeling all the possible states and transitions, also handling the communication and delegation between different components.

### Command Pattern

Encapsulates an operation execution, decoupling it from any caller component.

### Observer Pattern

Makes it possible to monitor changes on a given object, facilitating components communication without unnecessary depencencies.

### Factory Pattern

Creates different implementations of a base class based on input. This is key for our model-only flow logic (no ifs) approach.

## TODO

**APP**
- [ ] Create base module structure
- [ ] Add user interface lib
- [ ] Create application store and events
- [ ] Error events -> Messages Component

**CALCULATOR**
- [ ] Create events
- [ ] Create user interface
- [ ] Create factory abstraction
- [ ] Create commands abstraction

**TAPE**
- [ ] Create events
- [ ] Create user interface

**MESSAGES**
- [ ] Create events
- [ ] Create user interface